﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProAgil.Repository.Migrations
{
    public partial class PrecoLote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Proce",
                table: "Lote");

            migrationBuilder.AddColumn<decimal>(
                name: "Preco",
                table: "Lote",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Preco",
                table: "Lote");

            migrationBuilder.AddColumn<decimal>(
                name: "Proce",
                table: "Lote",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
