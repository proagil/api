using Microsoft.EntityFrameworkCore;
using ProAgil.Domain;
using System;
using System.Collections.Generic;

namespace ProAgil.Repository
{
    public class ProAgilContext : DbContext
    {
        public ProAgilContext(DbContextOptions<ProAgilContext> options) : base(options) { }

        public DbSet<Evento> Eventos { get; set; }

        public DbSet<Palestrante> Palestrantes { get; set; }
        public DbSet<PalestranteEvento> PalestranteEventos { get; set; }
        public DbSet<Lote> Lote { get; set; }
        public DbSet<RedeSocial> RedesSociais { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<PalestranteEvento>()
                .HasKey(PE => new { PE.EventoId, PE.PalestranteId });

            //builder.Entity<Evento>()
            //  .HasData(new List<Evento>{
            //        new Evento(1, "São Paulo", Convert.ToDateTime("21/01/2021"), "Angular e suas novidades", 350, "1º Lote", "" ),
            //        new Evento(2, "Santos", Convert.ToDateTime("22/01/2021"), "Angular e .Net Core", 250, "2º Lote", "" )
            //  });
        }
    }
}
