﻿using ProAgil.Domain;
using System.Threading.Tasks;

namespace ProAgil.Repository
{
    public interface IProAgilRepository
    {
        //GERAL
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        //EVENTOS
        Task<Evento[]> GetAllEventoAsyncByTema(string tema, bool includePalestrantes = false);
        Task<Evento[]> GetAllEventoAsync(bool includePalestrantes = false);
        Task<Evento> GetAllEventoAsyncById(int EventoId, bool includePalestrantes = false);

        //PALESTRANTE
        Task<Palestrante[]> GetAllPalestranteAsyncByName(string nome, bool includeEventos = false);
        Task<Palestrante> GetAllPalestranteAsyncById(int PalestranteId, bool includeEventos = false);
    }
}
