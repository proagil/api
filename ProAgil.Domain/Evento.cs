using System;
using System.Collections.Generic;

namespace ProAgil.Domain
{
    public class Evento
    {
        public Evento()
        {

        }
        public Evento(int id, string local, DateTime data, string tema, int qtdPessoas, string lote, string imageUrl)
        {
            this.Id = id;
            this.Local = local;
            this.DataEvento = data;
            this.Tema = tema;
            this.QtdPessoas = qtdPessoas;
            this.ImagemURL = imageUrl;
        }

        public int Id { get; set; }
        public string Local { get; set; }
        public DateTime DataEvento { get; set; }
        public string Tema { get; set; }
        public int QtdPessoas { get; set; }
        public string ImagemURL { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public List<Lote> Lotes { get; set; }
        public List<RedeSocial> RedesSociais { get; set; }
        public List<PalestranteEvento> PalestrantesEventos { get; set; }
    }
}
